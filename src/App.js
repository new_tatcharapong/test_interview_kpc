import React from 'react';
import InputFrom from './Components/inputForm';
import TableShow from './Components/tableShow';

function App() {
  return (
    <div className="App">
        <InputFrom>
        </InputFrom>
        <TableShow>
        </TableShow>
    </div>
  );
}

export default App;
