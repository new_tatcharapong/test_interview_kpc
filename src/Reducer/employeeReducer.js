
const listEmployee = [
    {
        id:1,
        titleName:"Mrs",
        firstName:"Ratvadee",
        lastName:"Sagarik",
        birthDay:"",
        nation:"THAI",
        citizenId:"",
        gender:"Female",
        prefixPhone:"+66",
        phoneNumber:"0879862651",
        passportNo:"",
        expectedSalary:"",
    },
    {
        id:2,
        titleName:"Mrs",
        firstName:"Kangsadan",
        lastName:"Poosee",
        birthDay:"",
        nation:"THAI",
        citizenId:"",
        gender:"Female",
        prefixPhone:"+66",
        phoneNumber:"0827794999",
        passportNo:"",
        expectedSalary:"",
    },
    {
        id:3,
        titleName:"Mrs",
        firstName:"Risa",
        lastName:"Tri",
        birthDay:"",
        nation:"AMERICAN",
        citizenId:"",
        gender:"Female",
        prefixPhone:"+66",
        phoneNumber:"0720229988",
        passportNo:"",
        expectedSalary:"",
    },
    {
        id:4,
        titleName:"Mr",
        firstName:"Attapon",
        lastName:"Sagarik",
        birthDay:"",
        nation:"LAOS",
        citizenId:"",
        gender:"Male",
        prefixPhone:"+66",
        phoneNumber:"0939077830",
        passportNo:"",
        expectedSalary:"",
    },
]

var dataInit = {
    listEmployee:[...listEmployee],
    editItem:{}
}

const employeeReducer =(state={...dataInit},action)=>{
    console.log(state)
    switch(action.type){
        case "Add":
            console.log("Add")
            var {data} = action;
            data.id = state.listEmployee.length + 1;
            var result = state.listEmployee.concat([data]);
            state.listEmployee = [...result]
            return Object.assign({}, state);

        case "Click Edit":
            state.editItem = state.listEmployee.find((item,index)=>item.id===action.data.id);
            return Object.assign({}, state);

        case "Save Change":
            const indexData = state.listEmployee.findIndex((item,index)=>item.id===action.data.id)
            state.listEmployee[indexData] = {...action.data};
            state.editItem = {};
            console.log(state.listEmployee)
            return Object.assign({}, state);  

        case "Delete Sigle":
            state.listEmployee = state.listEmployee.filter((item,index)=>item.id!==action.data.id)
            return Object.assign({}, state);

        case "Delete More":
            for(var i = 0;i<action.data.length;i++){
                state.listEmployee = state.listEmployee.filter((item,index)=>item.id!==action.data[i]);
            }
            return Object.assign({}, state);

        default:
            return Object.assign({}, state)
    }
}
export default employeeReducer;