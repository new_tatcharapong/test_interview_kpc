import React,{Component} from 'react';
import {Card,Row,Col,Input,Form,InputNumber, Button , Radio,Select,DatePicker , Modal} from 'antd';
import moment from 'moment';
import {connect} from 'react-redux';

const formItemLayoutRow1 = {
    title :{
        labelCol: {
            sm: { span: 24 },
            md: { span: 12 },
        },
        wrapperCol: {
            sm: { span: 24 },
            md: { span: 12 },
        },
    },
    Fname : {
        labelCol: {
            sm: { span: 24 },
            md: { span: 7 },
        },
        wrapperCol: {
            sm: { span: 24 },
            md: { span: 15 },
        },
    },
    Lname : {
        labelCol: {
            sm: { span: 24 },
            md: { span: 6 },
        },
        wrapperCol: {
            sm: { span: 24 },
            md: { span: 16 },
        },
    }
};

const formItemLayoutRow2 = {
    birthbay:{
        labelCol: {
            sm: { span: 24 },
            md: { span: 6 },
        },
        wrapperCol: {
            sm: { span: 24 },
            md: { span: 16 },
        },
    },
    nation:{
        labelCol: {
            sm: { span: 24 },
            md: { span: 6 },
        },
        wrapperCol: {
            sm: { span: 24 },
            md: { span: 16 },
        },
    }
};

const formItemLayoutRow3 = {
    labelCol: {
      sm: { span: 24 },
      md: { span: 2 },
    },
    wrapperCol: {
      sm: { span: 24 },
      md: { span: 22 },
    },
};

const formItemLayoutRow4 = {
    labelCol: {
      sm: { span: 24 },
      md: { span: 3 },
    },
    wrapperCol: {
      sm: { span: 24 },
      md: { span: 16 },
    },
};

const listConstTitle = ["Mr","Mrs","Miss"];
const listConstNation = ["THAI","AMERICAN","LAOS"];
const listConstGender = ["Male","Female","Unisex"];
const listConstPrefixPhone = ["+66"];

class inputFrom extends Component{

    constructor(props){
        super(props);
        this.state = {
            dataInput:{
                id:0,
                titleName:"",
                firstName:"",
                lastName:"",
                birthDay:"",
                nation:"",
                citizenId:"",
                gender:listConstGender[0],
                prefixPhone:listConstPrefixPhone[0],
                phoneNumber:"",
                passportNo:"",
                expectedSalary:"",
            },
            citizenIdInput:{
                0:"",
                1:"",
                2:"",
                3:"",
                4:"",
            },
            isEdit:false,
            isSubmit:false,
        }
    }

    componentWillReceiveProps = (nextProps) =>{
        if(nextProps.editItem&&nextProps.editItem!=={}){
            var {citizenIdInput} = this.state;
            if(nextProps.editItem.citizenId){
                citizenIdInput = {
                    0:nextProps.editItem.citizenId.substring(0, 1),
                    1:nextProps.editItem.citizenId.substring(1, 5),
                    2:nextProps.editItem.citizenId.substring(5, 10),
                    3:nextProps.editItem.citizenId.substring(10, 12),
                    4:nextProps.editItem.citizenId.substring(12, 13),
                }
            }
            this.setState({
                dataInput:nextProps.editItem,
                citizenIdInput,
                isEdit:true,
            },()=> console.log(this.state))
        }
    }

    fnValidateData =(data)=>{
        if(!data.titleName || !data.firstName || !data.lastName || !data.birthDay || !data.phoneNumber || !data.expectedSalary){
            return false;
        }
        return true;
    }

    fnOnSubmit =() =>{
        var {citizenIdInput,dataInput,isSubmit} = this.state;
        this.setState({
            isSubmit:true
        });
        for(var i in citizenIdInput){
            dataInput.citizenId += citizenIdInput[i]
        }
        if(!this.fnValidateData(dataInput)){
            Modal.error({
                title: 'Please input all required field.',
            });
        }
        else{
            this.props.dispatch({
                type:"Add",
                data:dataInput,
            })
            this.setState({
                dataInput:{
                    id:0,
                    titleName:"",
                    firstName:"",
                    lastName:"",
                    birthDay:"",
                    nation:"",
                    citizenId:"",
                    gender:listConstGender[0],
                    prefixPhone:listConstPrefixPhone[0],
                    phoneNumber:"",
                    passportNo:"",
                    expectedSalary:"",
                },
                isEdit:false,
                isSubmit:false,
                citizenIdInput:{
                    0:"",
                    1:"",
                    2:"",
                    3:"",
                    4:"",
                },
            })
        }
        console.log(this.state)
    }

    fnOnSaveChange =()=>{
        var {citizenIdInput,dataInput,isSubmit} = this.state;
        this.setState({
            isSubmit:true
        });
        for(var i in citizenIdInput){
            dataInput.citizenId += citizenIdInput[i]
        }
        if(!this.fnValidateData(dataInput)){
            Modal.error({
                title: 'Please input all required field.',
            });
        }
        else{
            this.props.dispatch({
                type:"Save Change",
                data:this.state.dataInput,
            })
            this.setState({
                dataInput:{
                    id:0,
                    titleName:"",
                    firstName:"",
                    lastName:"",
                    birthDay:"",
                    nation:"",
                    citizenId:"",
                    gender:listConstGender[0],
                    prefixPhone:listConstPrefixPhone[0],
                    phoneNumber:"",
                    passportNo:"",
                    expectedSalary:"",
                },
                isEdit:false,
                isSubmit:false,
                citizenIdInput:{
                    0:"",
                    1:"",
                    2:"",
                    3:"",
                    4:"",
                },
            })
        }
        console.log(this.state)
    }

    fnOnCancelChange = () =>{
        this.setState({
            dataInput:{
                id:0,
                titleName:"",
                firstName:"",
                lastName:"",
                birthDay:"",
                nation:"",
                citizenId:"",
                gender:listConstGender[0],
                prefixPhone:listConstPrefixPhone[0],
                phoneNumber:"",
                passportNo:"",
                expectedSalary:"",
            },
            isEdit:false,
            isSubmit:false,
            citizenIdInput:{
                0:"",
                1:"",
                2:"",
                3:"",
                4:"",
            },
        })
    }

    hasNumber = (myString) =>{
        //return /\d/.test(myString);
        return !isNaN(myString);
      }

    fnDataChange =(element,type,maxLength,inputNext)=>{
        var {dataInput,citizenIdInput} = this.state;
        var checkFormatNumber;
        switch(type){
            case "titleName":
                dataInput.titleName = element;
                break;
            case "firstName":
                dataInput.firstName = element.target.value;
                break;
            case "lastName":
                dataInput.lastName = element.target.value;
                break;
            case "birthDay":
                dataInput.birthDay = element ? element._d : "";
                break;
            case "nation":
                dataInput.nation = element;
                break;
            case "citizenId":
                checkFormatNumber = this.hasNumber(element.target.value);
                if(checkFormatNumber===true || !element.target.value){
                    if (element.target.value.length <= maxLength) {
                        citizenIdInput[inputNext-1] = element.target.value
                    }
                    if (element.target.value.length === maxLength){
                        if(inputNext===1){
                            this.nextComponent1.focus();
                        }
                        else if(inputNext===2){
                            this.nextComponent2.focus();
                        }
                        else if(inputNext===3){
                            this.nextComponent3.focus();
                        }
                        else if(inputNext===4){
                            this.nextComponent4.focus();
                        }
                        else{
    
                        }
                    } 
                }
                break;
            case "gender":
                dataInput.gender = element.target.value;
                break;
            case "prefixPhone":
                dataInput.prefixPhone = element;
                break;   
            case "phoneNumber":
                checkFormatNumber = this.hasNumber(element.target.value);
                if(checkFormatNumber || !element.target.value){
                    dataInput.phoneNumber = element.target.value;
                }
                break;
            case "passportNo":
                dataInput.passportNo = element.target.value;
                break;
            case "expectedSalary":
                dataInput.expectedSalary = element;
                break;
            default:
                break;
        }
        this.setState({
            dataInput:{...dataInput},
            citizenIdInput:{...citizenIdInput}
        })
    }


    render(){

        const {dataInput,isEdit,isSubmit,citizenIdInput} = this.state;
        console.log("Render")
        console.log(dataInput)
        return(
            <div className="site-card-wrapper" style={{marginTop:"10px"}}>
                <Row gutter={16}>
                <Col sm={1} md={6}></Col>
                <Col sm={22} md={12}>
                    <Card bordered>
                        <Row>
                            <Col sm={24} md={4}>
                                <Form.Item label="Title" {...formItemLayoutRow1.title} required validateStatus={(isSubmit&&!dataInput.titleName)?"error":"success"}>
                                    <Select value={dataInput.titleName} onChange={(element)=>this.fnDataChange(element,"titleName")}>
                                        {listConstTitle.map((item,index)=><Select.Option key={index} value={item}>{item}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col sm={24} md={10}>
                                <Form.Item label="Firstname" {...formItemLayoutRow1.Fname} required validateStatus={isSubmit&&!dataInput.firstName?"error":"success"}>
                                    <Input value={dataInput.firstName} onChange={(element)=>this.fnDataChange(element,"firstName")}></Input>
                                </Form.Item>
                            </Col>
                            <Col sm={24} md={10}>
                                <Form.Item label="Lastname" {...formItemLayoutRow1.Lname} required validateStatus={isSubmit&&!dataInput.lastName?"error":"success"}>
                                    <Input value={dataInput.lastName} onChange={(element)=>this.fnDataChange(element,"lastName")}></Input>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={24} md={8}>
                                <Form.Item label="Birthday" {...formItemLayoutRow2.birthbay} required validateStatus={isSubmit&&!dataInput.birthDay?"error":"success"}>
                                    <DatePicker key={dataInput.birthDay} defaultValue={dataInput.birthDay?moment(dataInput.birthDay):""} onChange={(element)=>this.fnDataChange(element,"birthDay")} format="MM/DD/YYYY" placeholder="mm/dd/yyyy" />
                                </Form.Item>
                            </Col>
                            <Col sm={24} md={9}>
                                <Form.Item label="Nationality" {...formItemLayoutRow2.nation}>
                                    <Select value={dataInput.nation} onChange={(element)=>this.fnDataChange(element,"nation")} placeholder="-- Please Select--">
                                        {listConstNation.map((item,index)=><Select.Option key={index} value={item}>{item}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={24} md={24}>
                                <Form.Item label="CitizenID" {...formItemLayoutRow3}>
                                    <Row>
                                        <Col md={2} sm={2}><Input value={citizenIdInput[0]} maxLength={1} style={{textAlign:"center"}} onChange={(e)=>this.fnDataChange(e,"citizenId",1,1)} placeholder="0"></Input></Col>
                                        <Col md={1} sm={1} style={{textAlign:"center"}}><h3>-</h3></Col>
                                        <Col md={2} sm={5}><Input value={citizenIdInput[1]} maxLength={4} ref={c => this.nextComponent1=c} onChange={(e)=>this.fnDataChange(e,"citizenId",4,2)} style={{textAlign:"center"}} placeholder="0000"></Input></Col>
                                        <Col md={1} sm={1} style={{textAlign:"center"}}><h3>-</h3></Col>
                                        <Col md={3} sm={6}><Input value={citizenIdInput[2]} maxLength={5} ref={c => this.nextComponent2=c} onChange={(e)=>this.fnDataChange(e,"citizenId",5,3)} style={{textAlign:"center"}} placeholder="00000"></Input></Col>
                                        <Col md={1} sm={1} style={{textAlign:"center"}}><h3>-</h3></Col>
                                        <Col md={2} sm={2}><Input value={citizenIdInput[3]} maxLength={2} ref={c => this.nextComponent3=c} onChange={(e)=>this.fnDataChange(e,"citizenId",2,4)} style={{textAlign:"center"}} placeholder="00"></Input></Col>
                                        <Col md={1} sm={1} style={{textAlign:"center"}}><h3>-</h3></Col>
                                        <Col md={2} sm={2}><Input value={citizenIdInput[4]} maxLength={1} ref={c => this.nextComponent4=c} onChange={(e)=>this.fnDataChange(e,"citizenId",1,5)} style={{textAlign:"center"}} placeholder="0"></Input></Col>
                                    </Row>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={24} md={16}>
                                <Form.Item label="Gender" {...formItemLayoutRow4} >
                                    <Radio.Group value={dataInput.gender} style={{width:"100%"}} onChange={(element)=>this.fnDataChange(element,"gender")}>
                                        {listConstGender.map((item,index)=><Radio key={index} value={item}>{item}</Radio>)}
                                    </Radio.Group>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={24} md={24}>
                                <Form.Item label="Mobile Phone" required validateStatus={isSubmit&&(!dataInput.phoneNumber||(dataInput.phoneNumber&&dataInput.phoneNumber.length!==10))?"error":"success"}>
                                    <Select  value={dataInput.prefixPhone} style={{textAlign:"center",width:"10%"}} onChange={(element)=>this.fnDataChange(element,"prefixPhone")}>
                                        {listConstPrefixPhone.map((item,index)=><Select.Option key={index} value={item}>{item}</Select.Option>)}
                                    </Select>
                                    <span style={{fontSize:18}}> - </span>
                                    <Input value={dataInput.phoneNumber} maxLength={10} style={{width:"30%"}} placeholder="0XXXXXXX21" onChange={(element)=>this.fnDataChange(element,"phoneNumber")}></Input>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={24} md={12}>
                                <Form.Item label="Passport No">
                                    <Input value={dataInput.passportNo} style={{width:"50%"}} onChange={(element)=>this.fnDataChange(element,"passportNo")} />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={24} md={12}>
                                <Form.Item label="Expected Salary" required validateStatus={isSubmit&&!dataInput.expectedSalary?"error":"success"}>
                                    <InputNumber value={dataInput.expectedSalary} style={{width:"70%"}} onChange={(element)=>this.fnDataChange(element,"expectedSalary")}/><span> THB</span>
                                </Form.Item>
                            </Col>
                            {isEdit===true?
                                <>
                                    <Col sm={24} md={10} style={{textAlign:"right"}}>
                                        <Button size="large" danger onClick={this.fnOnCancelChange} style={{marginRight:"10px"}}>Cancel</Button>
                                        <Button size="large" type="primary" ghost onClick={this.fnOnSaveChange}>Save Change</Button>
                                    </Col>
                                </>
                                :
                                <Col sm={24} md={10} style={{textAlign:"right"}}>
                                    <Button size="large" type="primary" ghost onClick={this.fnOnSubmit}>SUBMIT</Button>
                                </Col>
                            }
                        </Row>
                    </Card>
                </Col>
                <Col sm={1} md={6}></Col>
                </Row>
            </div>
            
        )
    }
}
const mapStateToProps = (state) =>{
    console.log(state)
    const {editItem} = state;
    return {
        editItem : editItem,
    }
}
export default connect(mapStateToProps)(inputFrom);