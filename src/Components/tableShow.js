import React,{Component} from "react";
import {Card,Row,Col,Table,Button, Modal} from "antd";
import { ExclamationCircleOutlined } from '@ant-design/icons';
import {connect} from 'react-redux';

class TableShow extends Component{

    constructor(props){
        super(props);
        this.state = {
            selectedRowKeys: [], // Check here to configure the default column
            loading: false,
            listEmployees:[]
        };
    }

    fnOnClickDeleteMore = () => {
        Modal.confirm({
            title: 'Do you want to delete selected items?',
            icon: <ExclamationCircleOutlined />,
            okText: 'Yes',
            cancelText: 'No',
            onOk:()=>{
                this.setState({ loading: true });
                // ajax request after empty completing
                const {selectedRowKeys} = this.state;
                setTimeout(() => {
                    this.setState({
                        selectedRowKeys: [],
                        loading: false,
                    },()=>{
                            this.props.dispatch({
                                type:"Delete More",
                                data:selectedRowKeys,
                            })
                        });
                }, 1000);
            },
            onCancel:()=>{
                
            }
          });
        
    };
    
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
        
    };

    fnOnClickEdit = (item) =>{
        this.props.dispatch({
            type:"Click Edit",
            data:item,
        })
    }

    fnOnClickDelete = (item) =>{
        Modal.confirm({
            title: 'Do you want to delete this item?',
            icon: <ExclamationCircleOutlined />,
            okText: 'Yes',
            cancelText: 'No',
            onOk:()=>{
                this.props.dispatch({
                    type:"Delete Sigle",
                    data:item,
                })
            },
            onCancel:()=>{

            }
          });
        
    }

    render(){
        const {selectedRowKeys,loading} = this.state;
        const rowSelection = {selectedRowKeys,onChange: this.onSelectChange,};
        const hasSelected = selectedRowKeys.length > 0;

        const columns = [
            {
              title: 'NAME',
              dataIndex: '#',
              width:"30%",
              render:(text,item)=>{
                  return item.firstName + " " + item.lastName;
              }
            },
            {
              title: 'GENDER',
              dataIndex: 'gender',
              width:"15%",
            },
            {
              title: 'MOBILE PHONE',
              dataIndex: '#',
              width:"20%",
              render:(text,item)=>{
                return "("+ item.prefixPhone + ")" + item.phoneNumber;
              }
            },
            {
                title: 'NATIONALITY',
                dataIndex: 'nation',
                width:"20%",
              },
            {
                title: '',
                dataIndex: '#',
                width:"15%",
                render:(text,item,index)=>{
                    return <><a onClick={()=>this.fnOnClickEdit(item)}>Edit</a> / <a onClick={(element)=>this.fnOnClickDelete(item)}>Delete</a></>
                }
            },
        ];

        return(
            <div className="site-card-wrapper" style={{marginTop:"10px"}}>
                <Row gutter={16}>
                <Col sm={1} md={6}></Col>
                <Col sm={22} md={12}>
                    <Card bordered>
                        <Row>
                            <Col sm={24} md={24}>
                               <div>
                                    <div style={{ marginBottom: 16 }}>
                                        <Button type="primary" onClick={this.fnOnClickDeleteMore} disabled={!hasSelected} loading={loading}>
                                            Delete
                                        </Button>
                                        <span style={{ marginLeft: 8 }}>
                                            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                                        </span>
                                    </div>
                                    <Table rowSelection={rowSelection} pagination columns={columns} dataSource={[...this.props.listEmployees]} rowKey={(record,index)=>record.id}/>
                                </div>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col sm={1} md={6}></Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state,props) =>{
    console.log(state)
    const {listEmployee} = state;
    return {
        listEmployees : [...listEmployee],
    }
}
export default connect(mapStateToProps)(TableShow);
