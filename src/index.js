import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import App from './App';
import { createStore } from 'redux';
import employeeReducer from './Reducer/employeeReducer';
import { Provider } from 'react-redux';

const store = new createStore(employeeReducer); //create store for app


ReactDOM.render(<Provider store={store}><App /></Provider>,document.getElementById('root'));

